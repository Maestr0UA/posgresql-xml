/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package postgretest;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLXML;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import postgretest.Book.*;


public class MyDatabase {
    List<Book> books;
    //List<Book> temp = new ArrayList();
    Connection c = null;
    Statement stmt = null;

    public MyDatabase() {
        initDB();
        createDB();
        createDBDescr();
        
        
        getBook(0);
        
    }
    
    
    
    
    private void initDB () {

        
        try {
            Class.forName("org.postgresql.Driver");
            c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/test_base", "postgres", "1234");
            stmt = c.createStatement();
            
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println(e.getClass().getName()+": "+e.getMessage());
            System.exit(0);
        }
        System.out.println("Opened database successfully");
        
    }
    public void getBook(int index) {
        // если первый раз получаем список книг
        if (index == 0) { 
            try {
                books = new ArrayList();
                ResultSet rs = stmt.executeQuery( "SELECT * FROM test ORDER BY \"ID_Book\"" );
                while ( rs.next() ) {
                    
                    SQLXML xmlSource = rs.getSQLXML("books");
                    
                    
                    Book book = new XmlDomRead(xmlSource.getBinaryStream()).parseBook();
                    
                    books.add(book);
                    
                   
                }
            } catch (SQLException ex) {
                Logger.getLogger(MyDatabase.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else //если выбрали конкретную книгу
            if (index > 0 && index < books.size()) {
                try {
                    String query = "SELECT * FROM test WHERE \"ID_Book\"="+index;
                    ResultSet rs = stmt.executeQuery(query);
                    while ( rs.next() ) {
                    
                        SQLXML xmlSource = rs.getSQLXML("books");
                        Book book = new XmlDomRead(xmlSource.getBinaryStream()).parseBook();
                        books.add(book);
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(MyDatabase.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        
        //System.out.println("length = "+books.size());
    }
       
    private void createDB () {
        try {
            stmt.executeUpdate("DROP TABLE test CASCADE");
        } catch (SQLException ex) {
            Logger.getLogger(MyDatabase.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("tast base doesn`t exist");
        } finally {
             try {
                 stmt.executeUpdate("CREATE TABLE test\n" +
                                    "(\n" +
                                    "  books xml,\n" +
                                    "  \"ID_Book\" serial NOT NULL,\n" +
                                    "  CONSTRAINT test_pkey PRIMARY KEY (\"ID_Book\")\n" +
                                    ")\n" +
                                    "WITH (OIDS=FALSE);\n" +
                                    "ALTER TABLE test OWNER TO postgres;");
                 
                 stmt.executeUpdate("INSERT INTO test VALUES (XMLPARSE (CONTENT '<book id=\"1\"><title>Властелин колец</title><author>Дж. Р. Р. Толкин</author></book>'))");
                 stmt.executeUpdate("INSERT INTO test VALUES (XMLPARSE (CONTENT '<book id=\"2\"><title>Мастер и Маргарита</title><author>Михаил Булгаков</author></book>'))");
                 stmt.executeUpdate("INSERT INTO test VALUES (XMLPARSE (CONTENT '<book id=\"3\"><title>Над пропастью во ржи</title><author>Джером Сэлинджер</author></book>'))");
                 stmt.executeUpdate("INSERT INTO test VALUES (XMLPARSE (CONTENT '<book id=\"4\"><title>Приключения Шерлока Холмса</title><author>Артур Конан Дойль</author></book>'))");
                 stmt.executeUpdate("INSERT INTO test VALUES (XMLPARSE (CONTENT '<book id=\"5\"><title>Преступление и наказание</title><author>Ф. М. Достоевский</author></book>'))");
                 stmt.executeUpdate("INSERT INTO test VALUES (XMLPARSE (CONTENT '<book id=\"6\"><title>Портрет Дориана Грея</title><author>Оскар Уайльд</author></book>'))");
                 stmt.executeUpdate("INSERT INTO test VALUES (XMLPARSE (CONTENT '<book id=\"7\"><title>Маленький принц</title><author>Антуан де Сент-Экзюпери</author></book>'))");
                 stmt.executeUpdate("INSERT INTO test VALUES (XMLPARSE (CONTENT '<book id=\"8\"><title>Собачье сердце</title><author>Михаил Булгаков</author></book>'))");
                 stmt.executeUpdate("INSERT INTO test VALUES (XMLPARSE (CONTENT '<book id=\"9\"><title>Три товарища</title><author>Эрих Мария Ремарк</author></book>'))");
                 stmt.executeUpdate("INSERT INTO test VALUES (XMLPARSE (CONTENT '<book id=\"10\"><title>Унесенные ветром</title><author>Маргарет Митчелл</author></book>'))");
                 stmt.executeUpdate("INSERT INTO test VALUES (XMLPARSE (CONTENT '<book id=\"11\"><title>Гордость и предубеждение</title><author>Джейн Остен</author></book>'))");
                 stmt.executeUpdate("INSERT INTO test VALUES (XMLPARSE (CONTENT '<book id=\"12\"><title>Герой нашего времени</title><author>Михаил Лермонтов</author></book>'))");
                 stmt.executeUpdate("INSERT INTO test VALUES (XMLPARSE (CONTENT '<book id=\"13\"><title>Парфюмер</title><author>Патрик Зюскинд</author></book>'))");
                 stmt.executeUpdate("INSERT INTO test VALUES (XMLPARSE (CONTENT '<book id=\"14\"><title>Джен Эйр</title><author>Шарлотта Бронте</author></book>'))");
                 stmt.executeUpdate("INSERT INTO test VALUES (XMLPARSE (CONTENT '<book id=\"15\"><title>Триумфальная арка</title><author>Эрих Мария Ремарк</author></book>'))");
                 stmt.executeUpdate("INSERT INTO test VALUES (XMLPARSE (CONTENT '<book id=\"16\"><title>Сто лет одиночества</title><author>Габриэль Гарсиа Маркес</author></book>'))");
                 stmt.executeUpdate("INSERT INTO test VALUES (XMLPARSE (CONTENT '<book id=\"17\"><title>Зеленая миля</title><author>Стивен Кинг</author></book>'))");
                 stmt.executeUpdate("INSERT INTO test VALUES (XMLPARSE (CONTENT '<book id=\"18\"><title>1984</title><author>Джордж Оруэлл   </author></book>'))");
                 stmt.executeUpdate("INSERT INTO test VALUES (XMLPARSE (CONTENT '<book id=\"19\"><title>12 стульев</title><author>Илья Ильф, Евгений Петров</author></book>'))");
                 stmt.executeUpdate("INSERT INTO test VALUES (XMLPARSE (CONTENT '<book id=\"20\"><title>451 градус по Фаренгейту</title><author>Рэй Брэдбери</author></book>'))");
                 stmt.executeUpdate("INSERT INTO test VALUES (XMLPARSE (CONTENT '<book id=\"21\"><title>Поющие в терновнике</title><author>Колин Маккалоу</author></book>'))");
                 stmt.executeUpdate("INSERT INTO test VALUES (XMLPARSE (CONTENT '<book id=\"22\"><title>Белый Клык</title><author>Джек Лондон</author></book>'))");
                 stmt.executeUpdate("INSERT INTO test VALUES (XMLPARSE (CONTENT '<book id=\"23\"><title>Робинзон Крузо</title><author>Даниель Дефо</author></book>'))");
                 stmt.executeUpdate("INSERT INTO test VALUES (XMLPARSE (CONTENT '<book id=\"24\"><title>Старик и море</title><author>Эрнест Хемингуэй</author></book>'))");
                 stmt.executeUpdate("INSERT INTO test VALUES (XMLPARSE (CONTENT '<book id=\"25\"><title>Идиот</title><author>Ф. М. Достоевский</author></book>'))");
                 stmt.executeUpdate("INSERT INTO test VALUES (XMLPARSE (CONTENT '<book id=\"26\"><title>Над кукушкиным гнездом</title><author>Кен Кизи</author></book>'))");
                 stmt.executeUpdate("INSERT INTO test VALUES (XMLPARSE (CONTENT '<book id=\"27\"><title>Три мушкетера</title><author>Александр Дюма</author></book>'))");
                 stmt.executeUpdate("INSERT INTO test VALUES (XMLPARSE (CONTENT '<book id=\"28\"><title>Мертвые души</title><author>Н. В. Гоголь</author></book>'))");
                 stmt.executeUpdate("INSERT INTO test VALUES (XMLPARSE (CONTENT '<book id=\"29\"><title>Ромео и Джульетта</title><author>Уильям Шекспир</author></book>'))");

             } catch (SQLException ex1) {
                 Logger.getLogger(MyDatabase.class.getName()).log(Level.SEVERE, null, ex1);
             }
        
            
        }  
    }      
    
    private void createDBDescr () {
        try {
            stmt.executeUpdate("DROP TABLE \"Book_Description\";");
        } catch (SQLException ex) {
            Logger.getLogger(MyDatabase.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
             try {
                 stmt.executeUpdate("CREATE TABLE \"Book_Description\"\n" +
                                    "(\n" +
                                    "  \"Descr\" text,\n" +
                                    "  \"ID_Descr\" integer NOT NULL,\n" +
                                    "  CONSTRAINT \"Book_Description_ID_Descr_fkey\" FOREIGN KEY (\"ID_Descr\")\n" +
                                    "      REFERENCES test (\"ID_Book\") MATCH SIMPLE\n" +
                                    "      ON UPDATE NO ACTION ON DELETE CASCADE\n" +
                                    ")\n" +
                                    "WITH (OIDS=FALSE);\n" +
                                    "ALTER TABLE \"Book_Description\" OWNER TO postgres;");
                 
                 stmt.executeUpdate("INSERT INTO \"Book_Description\" (\"Descr\", \"ID_Descr\") VALUES ('Толкиен рассказывает историю Кольца Всевластия, созданного давным-давным темным властелином Сауроном, чтобы подчинить себе весь мир. Кольцо попадает в руки к хоббиту Фродо от его дядюшки.',1)");
                 stmt.executeUpdate("INSERT INTO \"Book_Description\" (\"Descr\", \"ID_Descr\") VALUES ('Эта книга не поддается никакой классификации и не желает укладываться ни в какие рамки и схемы. Сатирически обрисованный быт, фантастика, мистика, теология, философия, лирика настолько прочно переплелись в нем, что не распутаешь, не отделишь одно от другого.',2)");
                 stmt.executeUpdate("INSERT INTO \"Book_Description\" (\"Descr\",\"ID_Descr\") VALUES ('История 17-летнего подростка, его восприятие мира, неприятие принятых в обществе принципов, канонов и морали.',3)");
                 stmt.executeUpdate("INSERT INTO \"Book_Description\" (\"Descr\",\"ID_Descr\") VALUES ('Приключения легендарного сыщика Шерлока Холмса, которые в большинстве случаев нам рассказывает его друг-спутник доктор Ватсон.',4)");
                 stmt.executeUpdate("INSERT INTO \"Book_Description\" (\"Descr\",\"ID_Descr\") VALUES ('Достоевский впервые показал возможность сочетания в одном человеке низкого и высокого, подлого и благородного, ничтожного и великого. Человек - это тайна за семью печатями, загадка, непостижимое. Мучительная внутренняя борьба бушует в Раскольникове.',5)");
                 stmt.executeUpdate("INSERT INTO \"Book_Description\" (\"Descr\",\"ID_Descr\") VALUES ('Однажды художник Бэзил Холлуорд нарисовал портрет молодого и красивого Дориана Грея, и Дориан влюбляется в самого себя. Он одержим мыслями о том, как было бы хорошо если бы вместо него старел портрет, а он сам вечно оставался таким же прекрасным. Но мысли материальны.',6)");
                 stmt.executeUpdate("INSERT INTO \"Book_Description\" (\"Descr\",\"ID_Descr\") VALUES ('\"Ведь все взрослые сначала были детьми, только мало кто из них об этом помнит\". Книга об искусстве взаимоотношений, о понимании, о неиспользованных возможностях, о верности самому себе.',7)");
                 stmt.executeUpdate("INSERT INTO \"Book_Description\" (\"Descr\",\"ID_Descr\") VALUES ('Талантливый хирург Филипп Филиппович Преображенский задумал необычный эксперимент. Он решает пересадить собаке человеческий гипофиз и яички.',8)");
                 stmt.executeUpdate("INSERT INTO \"Book_Description\" (\"Descr\",\"ID_Descr\") VALUES ('Книга-исповедь - честная, драматичная, искренняя, щемящая сердце история мужской дружбы, любви, отчаяния, борьбы, эмоций, чувств. Роман не нуждается в рецензии: это нужно читать.',9)");
                 stmt.executeUpdate("INSERT INTO \"Book_Description\" (\"Descr\",\"ID_Descr\") VALUES ('Глубокая, многогранная, написанная хорошим языком книга события которой происходят в 1860-х годах в США, в разгар гражданской войны и после нее. Это не дешевая история любви, это роман об Эпохе.',10)");
                 stmt.executeUpdate("INSERT INTO \"Book_Description\" (\"Descr\",\"ID_Descr\") VALUES ('История семьи Беннет, в которой родились одни девочки, состояние отца которых принадлежит дальнему родственнику, и в момент смерти главы семейства они по закону останутся ни с чем.',11)");
                 stmt.executeUpdate("INSERT INTO \"Book_Description\" (\"Descr\",\"ID_Descr\") VALUES ('Лермонтов испытывает Печорина в различных ситуациях, и прежде всего в любви. В романе рассказывается про отношения Печорина с тремя женщинами - Бела, Мэри и Вера.',12)");
                 stmt.executeUpdate("INSERT INTO \"Book_Description\" (\"Descr\",\"ID_Descr\") VALUES ('Гренуй - уникальный ребенок, родившейся у смердящей рыбной лавки, брошенный матерью, отвергнутый всеми в приюте, обладает острейшим обонянием.',13)");
                 stmt.executeUpdate("INSERT INTO \"Book_Description\" (\"Descr\",\"ID_Descr\") VALUES ('История о тяжелой судьбе сироты Джейн Эйр, ее детстве в доме тёти, которая ненавидит и все время унижает ее. Годы проведенные в школе-приюте. Работа гувернанткой. И любовь. Любовь к угрюмому, ироничному, уверенному в себе мистеру Рочестеру, хозяину поместья.',14)");
                 stmt.executeUpdate("INSERT INTO \"Book_Description\" (\"Descr\",\"ID_Descr\") VALUES ('История Равика, талантливого немецкого хирурга, который скрывается в Париже и зарабатывает на жизнь тем, что оперирует вместо французского хирурга-неумехи. История его отношения с актрисой Жоан, их любовь, ссоры, ночи.',15)");
                 stmt.executeUpdate("INSERT INTO \"Book_Description\" (\"Descr\",\"ID_Descr\") VALUES ('История вымышленного городка Макондо, основаного импульсивным и волевым Хосе Аркадио Буэндиа.',16)");
                 stmt.executeUpdate("INSERT INTO \"Book_Description\" (\"Descr\",\"ID_Descr\") VALUES ('История бывшего надзирателя тюрьмы Пола Эджкомба. Пол работал в тюремном блоке, в который попадали заключенные приговоренные к смерти.',17)");
                 stmt.executeUpdate("INSERT INTO \"Book_Description\" (\"Descr\",\"ID_Descr\") VALUES ('Роман описывает мир разделенный между тремя тоталитарным государставами. Книга о тотальном контроле, тотальном уничтожении всего человеческого и о попытках выжить в мире ненависти.',18)");
                 stmt.executeUpdate("INSERT INTO \"Book_Description\" (\"Descr\",\"ID_Descr\") VALUES ('Остап Бендер и Киса Воробьянинов занимаются поиском сокровищ, а именно бриллиантов, тёщи Кисы, спрятанных в одном из 12 стульев изящного гарнитура. Их уносит в круговорот из поисков, неудач, стараний и головокружительных авантюр...',19)");
                 stmt.executeUpdate("INSERT INTO \"Book_Description\" (\"Descr\",\"ID_Descr\") VALUES ('Книга о тоталитарном обществе, основные идеи которого - массовая культура и потребительское мышление. Главный герой, Гай Монтег, работает «пожарником», сжигает книги.',20)");
                 stmt.executeUpdate("INSERT INTO \"Book_Description\" (\"Descr\",\"ID_Descr\") VALUES ('На фоне австралийского быта, потрясающих картин природы воспеваются сильные и глубокие чувства к родным, к семье, к родной земле, ну и конечно же любовь мужчины и женщины, которую не может пересилить даже священный обет.',21)");
                 stmt.executeUpdate("INSERT INTO \"Book_Description\" (\"Descr\",\"ID_Descr\") VALUES ('Повесть, главным героем которой является волк по кличке Белый Клык. События разворачиваются на Аляске, в период золотой лихорадки, в конце 19 века.',22)");
                 stmt.executeUpdate("INSERT INTO \"Book_Description\" (\"Descr\",\"ID_Descr\") VALUES ('Книга построена в виде дневника моряка Робинзона Крузо, который 28 лет провел на необитаемом острове, после того как его судно потерпело крушение. Робинзон сталкивается с различными трудностями и находит адекватные а порой оригинальные пути их решения.',23)");
                 stmt.executeUpdate("INSERT INTO \"Book_Description\" (\"Descr\",\"ID_Descr\") VALUES ('Читая эту книгу чувствуешь соленый вкус моря на губах, ощущаешь, что руки режет леса. Маленький эпизод жизни незначитаельного человека. Но это только с точки зрения остального мира.',24)");
                 stmt.executeUpdate("INSERT INTO \"Book_Description\" (\"Descr\",\"ID_Descr\") VALUES ('Каждый герой созданный Достоевским - это особенный мир, богатый, насыщенный, противоречивый. Все черты доведены до крайней степени, когда персонаж становится чуть ли не пародией на свой тип. Фёдор Михаилович показывает две грани любви - христианское милосердие, любовь к ближнему и роковую страсть.',25)");
                 stmt.executeUpdate("INSERT INTO \"Book_Description\" (\"Descr\",\"ID_Descr\") VALUES ('Повествование ведется от лица пациента психиатрической больницы. Главный герой Патрик МакМёрфи - свободолюбивый бунтарь, попадает в психиатрическое отделение из тюрьмы, якобы симулировав свою болезнь, чтобы избежать каторжных работ.',26)");
                 stmt.executeUpdate("INSERT INTO \"Book_Description\" (\"Descr\",\"ID_Descr\") VALUES ('Приключения д’Артаньяна и его друзей. Дюма переносит нас в другую эпоху, эпоху дуэлей, смертельных предательств, вечной дружбы, преданности слуг, искреней и неповторимой любви, дворцовых интриг.',27)");
                 stmt.executeUpdate("INSERT INTO \"Book_Description\" (\"Descr\",\"ID_Descr\") VALUES ('Язык Гоголя - непростой, но в этом и есть что-то, что придает русской речи не только различные цвета, но и массу разнообразнейших оттенков. Эти длиннющие предложения, своеобразная манера и стиль: такой, как бы автор играл с отдельными словами, вызывают улыбку, удивление, да что угодно! но равнодушными точно не оставляют.',28)");
                 stmt.executeUpdate("INSERT INTO \"Book_Description\" (\"Descr\",\"ID_Descr\") VALUES ('История любви Ромео и Джульетты, детей из враждающих семей. \"Нет повести печальнее на свете, Чем повесть о Ромео и Джульетте.',29)");
                 
                 
                 
                 
                 

             } catch (SQLException ex1) {
                 Logger.getLogger(MyDatabase.class.getName()).log(Level.SEVERE, null, ex1);
             }
        
            
        } 

        
    
    }
       
    public String getBookDescription(int index) {
         try {
                    String query = "SELECT * FROM \"Book_Description\" WHERE \"ID_Descr\"=" + index;
                    ResultSet rs = stmt.executeQuery(query);
                    while ( rs.next() ) {
                    
                        String descr = rs.getString("Descr");
                        return descr;
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(MyDatabase.class.getName()).log(Level.SEVERE, null, ex);
                    
                }
         return "";
    }
    
    public boolean deleteBook(int index) {
        try {
            String query = "DELETE FROM test WHERE \"ID_Book\"=" + index;
            int del = stmt.executeUpdate(query);
            if (del == 1) {
                getBook(0);
                System.out.println("length = "+books.size());
                return true;
            }
            else return false;
        } catch (SQLException ex) {
            Logger.getLogger(MyDatabase.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }       
    }
    
    
    public boolean deleteDescr(int index) {
        try {
            //String query = "UPDATE \"Book_Description\" SET \"Descr\" =' ' WHERE \"ID_Descr\" = " + index;
            String query = "DELETE FROM \"Book_Description\" WHERE \"ID_Descr\"=" + index;
            int del = stmt.executeUpdate(query);
            if (del == 1) {
                return true;
            }
            else return false;
        } catch (SQLException ex) {
            Logger.getLogger(MyDatabase.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }       
    }
    
    
    
    public void addBook(String title, String author) {
        Book book = new Book(title, author);
        int id = new Integer(books.get(books.size()-1).getId())+1;
        book.setId(new Integer(id).toString());
        
        try {
            books.add(book);
            String query = "INSERT INTO test VALUES (XMLPARSE (CONTENT '<book id=\"" + book.getId()+"\"><title>"+ book.getTitle()+ "</title><author>" + book.getAuthor()+ "</author></book>'))";
            stmt.executeUpdate(query);
            getBook(0);
            System.out.println("book added");
        } catch (SQLException ex) {
            Logger.getLogger(MyDatabase.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public boolean doSearch(String searchStr) {
        try {
            String query = "SELECT * from test WHERE XMLSERIALIZE (CONTENT \"books\" AS text) LIKE '%"+searchStr+"%'";
            System.out.println(query);
            ResultSet rs = stmt.executeQuery(query);
            
                

            while ( rs.next() ) {
                if (rs.isFirst()) {
                    books = new ArrayList();
                }
                SQLXML xmlSource = rs.getSQLXML("books");
                Book book = new XmlDomRead(xmlSource.getBinaryStream()).parseBook();
                books.add(book);
                
            }
            return true;
            } catch (SQLException ex) {
                Logger.getLogger(MyDatabase.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    
    public void editDBDescr (String descr, String id) {
       try {
           String query = "UPDATE \"Book_Description\" SET \"Descr\" ='"+descr+"' WHERE \"ID_Descr\" = \'"+id+"'";
           stmt.executeUpdate(query);
       } catch (SQLException ex1) {
           Logger.getLogger(MyDatabase.class.getName()).log(Level.SEVERE, null, ex1);
       }
    }
    
    
    
    
    public void editBook(Book book) {
        try {
            String query = "UPDATE test SET \"books\"=(XMLPARSE (CONTENT '<book id=\""+book.getId()+"\"><title>"+book.getTitle()+"</title><author>"+book.getAuthor()+"</author></book>')) WHERE \"ID_Book\"="+book.getId()+";";
            stmt.executeUpdate(query);
            String i = book.getId();
            //books.set(new Integer(book.getId()).intValue(), book);
            getBook(0);
            
        } catch (SQLException ex) {
            Logger.getLogger(MyDatabase.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void addDBDescr (String descr, int id) {
        try {
            stmt.executeUpdate("INSERT INTO \"Book_Description\" (\"Descr\",\"ID_Descr\") VALUES ('"+descr+"',"+id+")");
        } catch (SQLException ex1) {
            Logger.getLogger(MyDatabase.class.getName()).log(Level.SEVERE, null, ex1);
        }
        
            
    } 

        
    
    
    
}

    