package postgretest;
 
import java.awt.*;
import java.awt.event.*;
import java.beans.*;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
 
class CustomDialog extends JDialog
        implements ActionListener,
        PropertyChangeListener {
 
    static class Demo {
 
        public static void main(String[] args) {
            javax.swing.SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    JFrame frame = new JFrame("DialogDemo");
                    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                    frame.setLayout(new BorderLayout());
                    JButton dlg = new JButton("show dialog");
                    frame.add(dlg);
                    final CustomDialog customDialog = new CustomDialog(frame, "geisel");
                    customDialog.pack();
                    customDialog.setLocationRelativeTo(frame);
                    dlg.addMouseListener(new MouseListener() {
                        @Override
                        public void mouseClicked(MouseEvent e) {
                            customDialog.setVisible(true);
 
                        }
 
                        @Override
                        public void mousePressed(MouseEvent e) {
                        }
 
                        @Override
                        public void mouseReleased(MouseEvent e) {
                        }
 
                        @Override
                        public void mouseEntered(MouseEvent e) {
                        }
 
                        @Override
                        public void mouseExited(MouseEvent e) {
                        }
                    });
                    frame.setVisible(true);
                    frame.pack();
 
                }
            });
        }
    }
    private String typedTitle = null;

    public String getTypedTitle() {
        return typedTitle;
    }

    public void setTypedTitle(String typedTitle) {
        this.typedTitle = typedTitle;
    }

    public String getTypedAuthor() {
        return typedAuthor;
    }

    public void setTypedAuthor(String typedAuthor) {
        this.typedAuthor = typedAuthor;
    }
    private String typedAuthor = null;
    private JTextField textField1;
    private JLabel label1;
    private JLabel label2;
    private JTextField textField2;
    private String magicWord;
    private JOptionPane optionPane;
    private String btnString1 = "Enter";
    private String btnString2 = "Cancel";
 
    
 
    public CustomDialog(Frame aFrame, String aWord) {
        super(aFrame, true);
 
        
        setTitle(aWord);
 
        textField1 = new JTextField(10);
        textField2 = new JTextField(10);
        label1 = new JLabel("Title");
        label2 = new JLabel("Author");
 
        
        String msgString1 = aWord;
        
        Object[] array = {msgString1, label1, textField1, label2, textField2};
 
        Object[] options = {btnString1, btnString2};
 
        //Create the JOptionPane.
        optionPane = new JOptionPane(array,
                JOptionPane.QUESTION_MESSAGE,
                JOptionPane.YES_NO_OPTION,
                null,
                options,
                options[0]);
 
        //Make this dialog display it.
        setContentPane(optionPane);
 
        //Handle window closing correctly.
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent we) {
                /*
                 * Instead of directly closing the window,
                 * we're going to change the JOptionPane's
                 * value property.
                 */
                optionPane.setValue(new Integer(
                        JOptionPane.CLOSED_OPTION));
            }
        });
 
        //Ensure the text field always gets the first focus.
        addComponentListener(new ComponentAdapter() {
            public void componentShown(ComponentEvent ce) {
                textField1.requestFocusInWindow();
            }
        });
 
        addComponentListener(new ComponentAdapter() {
            public void componentShown(ComponentEvent ce) {
                textField2.requestFocusInWindow();
            }
        });
 
        //Register an event handler that puts the text into the option pane.
        textField1.addActionListener(this);
        textField2.addActionListener(this);
 
        //Register an event handler that reacts to option pane state changes.
        optionPane.addPropertyChangeListener(this);
    }
 
    /**
     * This method handles events for the text field.
     */
    public void actionPerformed(ActionEvent e) {
        optionPane.setValue(btnString1);
    }
 
    /**
     * This method reacts to state changes in the option pane.
     */
    public void propertyChange(PropertyChangeEvent e) {
        String prop = e.getPropertyName();
 
        if (isVisible()
                && (e.getSource() == optionPane)
                && (JOptionPane.VALUE_PROPERTY.equals(prop)
                || JOptionPane.INPUT_VALUE_PROPERTY.equals(prop))) {
            Object value = optionPane.getValue();
 
            if (value == JOptionPane.UNINITIALIZED_VALUE) {
                //ignore reset
                return;
            }
 
            //Reset the JOptionPane's value.
            //If you don't do this, then if the user
            //presses the same button next time, no
            //property change event will be fired.
            optionPane.setValue(
                    JOptionPane.UNINITIALIZED_VALUE);
 
            if (btnString1.equals(value)) {
                
                typedTitle = textField1.getText();
                typedAuthor = textField2.getText();
                if (!typedTitle.equals("") && typedTitle != null)
                    clearAndHide();
               
            } else { //user closed dialog or clicked cancel
                typedTitle = null;
                typedAuthor = null;
                clearAndHide();
            }
        }
    }
 
    
    public void clearAndHide() {
        textField1.setText(null);
        textField2.setText(null);
        setVisible(false);
    }
}