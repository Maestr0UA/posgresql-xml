/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package postgretest;

/**
 *
 * @author Xoxol
 */
public class Book {
        private String id;
        private String title;
        private String author;
        private String descr;

    public Book() {
        
    }
    
    public Book (String title) {
        this(title, "");
    }
    
    public Book (String title, String author) {
        if (!title.equals(""))
            this.title = title;
        else this.title = "";
        if (!author.equals(""))
            this.author = author;
        else this.author = "";
    }
    
    
    
    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the author
     */
    public String getAuthor() {
        return author;
    }

    /**
     * @param author the author to set
     */
    public void setAuthor(String author) {
        this.author = author;
    }
    
    
}
