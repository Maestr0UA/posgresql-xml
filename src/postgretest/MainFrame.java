/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package postgretest;

import java.awt.Color;
import java.awt.Component;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;


/**
 *
 * @author Xoxol
 */
public class MainFrame extends javax.swing.JFrame {
    String [] rowNames = {"ID", "Title", "Author"};
    JPopupMenu popupMenu;
    MyDatabase DB = null;
    Book newBook = null;
    javax.swing.JFrame main;
    String searchStr;
    JMenuItem descrItem = new JMenuItem("");
    Boolean changeDescr = false;
    String descrText = "";
    String descr = "";

    public MainFrame(String title) {
        super(title);
        DB = new MyDatabase();
        main = this;
        
        JMenuItem editItem = new JMenuItem("добавить новую книгу");
        JMenuItem deleteItem = new JMenuItem("Удалить книгу");
        JMenuItem addItem = new JMenuItem("редактировать");
        
        setLocationByPlatform(true);
        
        initComponents();
        jTable.addMouseListener(new MouseAdapter () {

            @Override
            public void mouseClicked(MouseEvent e) {
                
                if (e.getClickCount() == 2) {
                    Point p = e.getPoint();
                    int row = jTable.rowAtPoint(p);
                    String temp = (String) jTable.getValueAt(row, 0);
                    int id = new Integer(temp).intValue();
                    String descr = DB.getBookDescription(id);
                    ViewDescr.setText(descr);
                    ViewDescr.setEditable(false);
                    ViewDescr.setBackground(new Color(240,240,240));
                    ViewDescr.setVisible(true);
                    System.out.println("column = " + row);
                }
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                
                showPopupMenu(e);
            }
            
            void showPopupMenu (MouseEvent e) {
                 if (e.isPopupTrigger()) {
                    Point p = e.getPoint();
                    jTable.changeSelection(jTable.rowAtPoint(p), jTable.columnAtPoint(p), false, false);
                    String id = (String) jTable.getValueAt(jTable.getSelectedRow(), 0);
                    descrText = DB.getBookDescription(new Integer(id).intValue());
                    if (!descrText.equals(""))
                        descrItem.setText("Удалить описание");
                    else 
                        descrItem.setText("Добавить описание");
                    popupMenu.show(e.getComponent(), e.getX(), e.getY());
                }
            }
            
        
        });
        
        popupMenu = new JPopupMenu();
        
        editItem.addActionListener(new ActionListener () {

            @Override
            public void actionPerformed(ActionEvent e) {
                
              
                CustomDialog customDialog = new CustomDialog(main, "Edit new book");
                customDialog.pack();
                customDialog.setLocationRelativeTo(main);
                customDialog.setVisible(true);
                
                
                if (customDialog.getTypedTitle() != null && !customDialog.getTypedTitle().equals("")) {
                    if (customDialog.getTypedAuthor()!= null && !customDialog.getTypedAuthor().equals("")) {
                
                        DB.addBook(customDialog.getTypedTitle(), customDialog.getTypedAuthor());
                        
                        
                    }
                    else { 
                        DB.addBook(customDialog.getTypedTitle(), "unknown");
                
                    }
                    jTable.revalidate();
                    //jTable.repaint();
                }
                System.out.println("edit book = " + jTable.getSelectedRow());
            }
        
        });
        popupMenu.add(editItem);
        
        
        deleteItem.addActionListener(new ActionListener () {

            @Override
            public void actionPerformed(ActionEvent e) {
                String id = (String) jTable.getValueAt(jTable.getSelectedRow(), 0);
                if (DB.deleteBook(new Integer(id)))
                {
                    jTable.repaint();
                    System.out.println("delete book");
                }
            }
        
        });        
        popupMenu.add(deleteItem);
        
        
        descrItem.addActionListener(new ActionListener () {

            @Override
            public void actionPerformed(ActionEvent e) {
                String id = (String) jTable.getValueAt(jTable.getSelectedRow(), 0);
                if (descrText.equals("")) {
                    ViewDescr.setEditable(true);
                    ViewDescr.setBackground(Color.WHITE);
                    ViewDescr.setCaretPosition(0);
                    descrButton.setEnabled(true);
                } else {
                    DB.deleteDescr(new Integer(id).intValue());
                    ViewDescr.setEditable(false);
                    ViewDescr.setBackground(new Color(240,240,240));
                    descrText = "";
                }
                System.out.println("descr");
            }
            
        
        });        
        popupMenu.add(descrItem);

        
        
        addItem.addActionListener(new ActionListener () {

            @Override
            public void actionPerformed(ActionEvent e) {
              
                CustomDialog customDialog = new CustomDialog(main, "Change a book");
                customDialog.pack();
                customDialog.setLocationRelativeTo(main);
                customDialog.setVisible(true);
                Book book;
                
                String id = (String) jTable.getValueAt(jTable.getSelectedRow(), 0);
                if (customDialog.getTypedTitle() != null && !customDialog.getTypedTitle().equals("")) {
                    if (customDialog.getTypedAuthor()!= null && !customDialog.getTypedAuthor().equals("")) {
                        book = new Book(customDialog.getTypedTitle(), customDialog.getTypedAuthor());
                        
                        book.setId(new Integer(id).toString());
                        
                    }
                    else { 
                        book = new Book(customDialog.getTypedTitle(), "unknown");
                        book.setId(new Integer(id).toString());
                    }
                    DB.editBook(book);
                    book = null;
                    jTable.repaint();
                }
                                
                System.out.println("add book");
            }
        
        });           
        popupMenu.add(addItem);        

        
        jTable.add(popupMenu);
        jTable.getColumnModel().getColumn(0).setMinWidth(20);
        jTable.getColumnModel().getColumn(0).setMaxWidth(20); 
        ViewDescr.setLineWrap(true);
        ViewDescr.setWrapStyleWord(true);
        
    }
    
    

    

    
    
    public TableModel createTable() {
    
        return null;
    }
    
    public class myDataModel extends DefaultTableModel {

        public myDataModel(Object[][] data, Object[] columnNames) {
            super(data, columnNames);
        }

            
        
        @Override
        public int getRowCount() {
            
            return DB.books.size();
        }

        @Override
        public boolean isCellEditable(int row, int column) {
            return false;
        }
        
        
        
        @Override
        public int getColumnCount() {
            return rowNames.length;
        }

        @Override
        public Object getValueAt(int row, int col) {
            Book book = DB.books.get(row);
            switch (col) {
                case 0: return book.getId();
                case 1: return book.getTitle();
                case 2: return book.getAuthor();
                default: return "";
            
            }
            
        }
        
    
    }
    
    
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTable = new javax.swing.JTable();
        SearchButton = new javax.swing.JButton();
        SearchField = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        ViewDescr = new javax.swing.JTextArea();
        descrButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setLocationByPlatform(true);
        setName("Test Postgre"); // NOI18N
        setPreferredSize(new java.awt.Dimension(900, 570));
        setResizable(false);
        addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                formMouseClicked(evt);
            }
        });

        jTable.setModel(new myDataModel(null, rowNames));
        jScrollPane2.setViewportView(jTable);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        SearchButton.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        SearchButton.setText("SEARCH");
        SearchButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                SearchButtonMouseClicked(evt);
            }
        });

        SearchField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                SearchFieldKeyReleased(evt);
            }
        });

        ViewDescr.setEditable(false);
        ViewDescr.setBackground(new java.awt.Color(240, 240, 240));
        ViewDescr.setColumns(20);
        ViewDescr.setFont(new java.awt.Font("Monospaced", 0, 18)); // NOI18N
        ViewDescr.setRows(5);
        ViewDescr.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ViewDescrMouseClicked(evt);
            }
        });
        ViewDescr.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                ViewDescrKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(ViewDescr);

        descrButton.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        descrButton.setText("SAVE");
        descrButton.setEnabled(false);
        descrButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                descrButtonMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(SearchField, javax.swing.GroupLayout.PREFERRED_SIZE, 319, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(SearchButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 395, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(descrButton, javax.swing.GroupLayout.PREFERRED_SIZE, 274, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(67, 67, 67))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 452, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(descrButton, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(SearchButton, javax.swing.GroupLayout.DEFAULT_SIZE, 36, Short.MAX_VALUE)
                            .addComponent(SearchField))
                        .addGap(18, 18, 18)
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );

        getAccessibleContext().setAccessibleDescription("");

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void SearchButtonMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_SearchButtonMouseClicked
        searchStr = SearchField.getText();
        if (searchStr != null && !searchStr.equals("")) {
            if (DB.doSearch(searchStr)) {
                if (DB.books.size() > 0) {
                    jTable.revalidate();
                }
            } else
                JOptionPane.showInternalMessageDialog(main.getContentPane(),"Incorrect input", "Search Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_SearchButtonMouseClicked

    private void SearchFieldKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_SearchFieldKeyReleased
        if (SearchField.getText().equals("")) {
            DB.getBook(0);
            jTable.revalidate();
        }
    }//GEN-LAST:event_SearchFieldKeyReleased

    private void ViewDescrMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ViewDescrMouseClicked
        if (evt.getClickCount() == 2) {
            ViewDescr.setBackground(Color.WHITE);
            ViewDescr.setEditable(true);
            ViewDescr.setCaretColor(Color.red);
            descrButton.setEnabled(true);
            
        }
    }//GEN-LAST:event_ViewDescrMouseClicked

    private void ViewDescrKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_ViewDescrKeyPressed
        if (! changeDescr)
            changeDescr = true;
        
    }//GEN-LAST:event_ViewDescrKeyPressed

    private void formMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_formMouseClicked

    }//GEN-LAST:event_formMouseClicked

    private void descrButtonMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_descrButtonMouseClicked
        changeDescr = false;
        descrButton.setEnabled(false);
        String id = (String) jTable.getValueAt(jTable.getSelectedRow(), 0);
        System.out.println("descr " + descrText);
        
        if (!descrText.equals(""))
            DB.editDBDescr(ViewDescr.getText(),  id);
        else
            DB.addDBDescr(ViewDescr.getText(), new Integer(id).intValue());
    }//GEN-LAST:event_descrButtonMouseClicked

   
   
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton SearchButton;
    private javax.swing.JTextField SearchField;
    private javax.swing.JTextArea ViewDescr;
    private javax.swing.JButton descrButton;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable jTable;
    // End of variables declaration//GEN-END:variables
}
