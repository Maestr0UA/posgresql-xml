/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package postgretest;

/**
 *
 * @author Xoxol
 */
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class XmlDomRead {
    Document doc;
    
    public XmlDomRead(InputStream result){
        try {
            DocumentBuilderFactory f = DocumentBuilderFactory.newInstance();
            f.setValidating(false);
            f.setIgnoringElementContentWhitespace(true);
            DocumentBuilder builder = f.newDocumentBuilder();
            //doc = builder.parse(XmlDomRead.class.getResourceAsStream("test.xml"));
            doc = builder.parse(result);
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(XmlDomRead.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SAXException ex) {
            Logger.getLogger(XmlDomRead.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(XmlDomRead.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
    }

    public List<Book> parse() {
        List<Book> books = new ArrayList<>();
        
        
        Element root = doc.getDocumentElement();
        for (Node child = root.getFirstChild(); child != null; child = child.getNextSibling()) {
            if (child.getAttributes() != null) {
                Book book = new Book();
                book.setId(child.getAttributes().getNamedItem("id").getNodeValue());
                System.out.println(book.getId());
                
                for (Node grandson = child.getFirstChild(); grandson != null; grandson =  grandson.getNextSibling()) {
                    if (grandson.getAttributes() != null) {
                        switch (grandson.getNodeName()) {
                            case "title":
                                book.setTitle(grandson.getTextContent());
                                System.out.println(book.getTitle());
                                break;
                            case "author":
                                book.setAuthor(grandson.getTextContent());
                                System.out.println(book.getAuthor());
                                break;
                        
                        }
                        
                    }
                }
                
                books.add(book);
            }
        }
        return books;
    
    }
    
    
    public Book parseBook() {
        Book book = new Book();
        
        Element root = doc.getDocumentElement();
                
        book.setId(root.getAttributes().getNamedItem("id").getNodeValue());
       // System.out.println(book.getId());
                
        for (Node child = root.getFirstChild(); child != null; child =  child.getNextSibling()) {
            if (child.getAttributes() != null) {
                switch (child.getNodeName()) {
                    case "title":
                        book.setTitle(child.getTextContent());
                        //System.out.println(book.getTitle());
                        break;
                    case "author":
                        book.setAuthor(child.getTextContent());
                        //System.out.println(book.getAuthor());
                        break;
                        
                    }
                        
                }
        }
                
        return book;
    
    }
    
    
}
